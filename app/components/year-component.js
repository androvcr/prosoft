'use strict';

angular.
  module('ProSoftFrontend')
  .component('yearComponent', {
    restrict: 'E',
    templateUrl: 'app/components/year-component.html',
    controllerAs: 'vm',
    controller: 'yearComponentController',
    bindings: {
      holidayProvider: '='
    }
  })
  .controller('yearComponentController', ['$timeout', function (
      $timeout
    ) {
    // Extending controller
    let vm = this;
    let mounts = [];

    $timeout(() => {
      let startDay = moment(`0101${vm.holidayProvider.id}`);
      let endDay = moment(`1231${vm.holidayProvider.id}`);
    });

  }]);