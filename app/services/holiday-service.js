angular.
  module('ProSoftFrontend').
  factory('HolidayService', [
    '$q', 
    '$http',
    function(
      $q,
      $http
    ) {
      let service = {};
      let testKey = '286ee596-7ef9-4ec8-9cc4-332090acaa2f';
      let liveKey = 'b56719be-833f-4752-a914-3f84c8d30675';

      let base = 'https://holidayapi.com/v1/holidays';

      let _countryCodes = [{code: 'AR', name: 'Argentina'},
        {code: 'AO', name: 'Angola'},
        {code: 'AT', name: 'Austria'},
        {code: 'AU', name: 'Australia'},
        {code: 'AW', name: 'Aruba'},
        {code: 'BE', name: 'Belgium'},
        {code: 'BG', name: 'Bulgaria'},
        {code: 'BO', name: 'Bolivia'},
        {code: 'BR', name: 'Brazil'},
        {code: 'CA', name: 'Canada'},
        {code: 'CH', name: 'Switzerland'},
        {code: 'CN', name: 'China'},
        {code: 'CO', name: 'Colombia'},
        {code: 'CZ', name: 'Czech Republic'},
        {code: 'DE', name: 'Germany'},
        {code: 'DK', name: 'Denmark'},
        {code: 'DO', name: 'Dominican Republic'},
        {code: 'EC', name: 'Ecuador'},
        {code: 'ES', name: 'Spain'},
        {code: 'FR', name: 'France'},
        {code: 'GB', name: 'United Kingdom'},
        {code: 'GB-ENG', name: 'England'},
        {code: 'GB-NIR', name: 'Northern Ireland'},
        {code: 'GB-SCT', name: 'Scotland'},
        {code: 'GB-WLS', name: 'Wales'},
        {code: 'GR', name: 'Greece'},
        {code: 'GT', name: 'Guatemala'},
        {code: 'HN', name: 'Honduras'},
        {code: 'HR', name: 'Croatia'},
        {code: 'HU', name: 'Hungary'},
        {code: 'ID', name: 'Indonesia'},
        {code: 'IE', name: 'Ireland'},
        {code: 'IN', name: 'India'},
        {code: 'IL', name: 'Israel'},
        {code: 'IT', name: 'Italy'},
        {code: 'KZ', name: 'Kazakhstan'},
        {code: 'LS', name: 'Lesotho'},
        {code: 'LU', name: 'Luxembourg'},
        {code: 'MG', name: 'Madagascar'},
        {code: 'MQ', name: 'Martinique'},
        {code: 'MT', name: 'Malta'},
        {code: 'MU', name: 'Mauritius'},
        {code: 'MX', name: 'Mexico'},
        {code: 'MZ', name: 'Mozambique'},
        {code: 'NL', name: 'Netherlands'},
        {code: 'NO', name: 'Norway'},
        {code: 'PE', name: 'Peru'},
        {code: 'PK', name: 'Pakistan'},
        {code: 'PH', name: 'Philippines'},
        {code: 'PL', name: 'Poland'},
        {code: 'PR', name: 'Puerto Rico'},
        {code: 'PT', name: 'Portugal'},
        {code: 'PY', name: 'Paraguay'},
        {code: 'RE', name: 'Réunion'},
        {code: 'RU', name: 'Russia'},
        {code: 'SC', name: 'Seychelles'},
        {code: 'SE', name: 'Sweden'},
        {code: 'SG', name: 'Singapore'},
        {code: 'SI', name: 'Slovenia'},
        {code: 'ST', name: 'Sao Tome and Principe'},
        {code: 'SK', name: 'Slovakia'},
        {code: 'TR', name: 'Turkey'},
        {code: 'UA', name: 'Ukraine'},
        {code: 'US', name: 'United States'},
        {code: 'UY', name: 'Uruguay'},
        {code: 'VE', name: 'Venezuela'}
      ];

      function _getYearHolidays (country, year) {
          let config = {
            params: {
              key: testKey,
              country: country,
              year: year
            }
          };
          return $http.get(base, config);
      };

      service.getHolidays = _getYearHolidays;
      service.countryCodes = _countryCodes;

      return service;
    }]);