var ProSoftFrontend = angular.module('ProSoftFrontend', []);

ProSoftFrontend.controller('mainAppController', [
  '$q',
	'$scope',
	'HolidayService',
	function mainAppController(
    $q,
		$scope,
    HolidayService
  ) {
	let vm = this;
  vm.editParams = true;
  vm.countryCodes = HolidayService.countryCodes;

  vm.country = 'US';
  vm.startDate = moment('20080101').toDate();
  vm.endDate = undefined;
  vm.days = 85;

  vm.calendarContents = undefined;

  function processHolidays (data) {
    let tempStart = moment(vm.startDate);
    let tempEnd = moment(vm.endDate);
    let total = tempEnd.diff(tempStart, 'months', false)+1;

    vm.calendarContents = [];
    for (let i = 0; i <= total; i++) {
      //let temp = '1/' + (tempStart.month() + 1) + '/' + tempStart.year() ;
      let temp = `${tempStart.year()}${tempStart.month() < 10 ? '0'+(tempStart.month()+1) : (tempStart.month()+1)}01`;
      console.log(temp);
      let initMonth = _.cloneDeep(moment(temp));
      let nextMonth = _.cloneDeep(initMonth.add(1, 'month')); 
      let endMonth = _.cloneDeep(nextMonth.add(-1, 'day'));

      if (endMonth > tempEnd) {
        endMonth = tempEnd
      }

      vm.calendarContents.push({
        startDate: tempStart,
        endDate: endMonth,
      });

      tempStart = _.cloneDeep(nextMonth);
    }

    let hola = [];

    _.each(data, (element, key) => {
      hola.push({
        id: key,
        holidays: element.data.holidays,
        startDate: vm.startDate,
        endDate: vm.endDate
      });
    });

    console.log(vm.calendarContents);
  };

  function _getHolidays () {
    let startYear = moment(vm.startDate).year();
    vm.endDate = moment(vm.startDate).add(vm.days, 'days');
    let endYear = vm.endDate.year();

    let promiseData = {};
    promiseData[startYear] = HolidayService.getHolidays(vm.country, startYear);

    for (let i = startYear+1; i <= endYear; i++) {
      promiseData[i] = HolidayService.getHolidays(vm.country, i);
    }

    $q.all(promiseData)
      .then((data) => {
        processHolidays(data);  
      }).catch(() => {

      }).finally(() => {

      });
  }

  vm.go = function go () {
    _getHolidays();
  };

}]);